﻿using System.IO;
using System.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Cli
{
    public class Program
    {
        public static ILookup<string, string> RuleReference = new List<(string, string)> {
            ("", "Unknown"),
            ("SCS0001", "Command Injection"),
            ("SCS0002", "SQL Injection"),
            ("SCS0003", "XPath Injection"),
            ("SCS0004", "Certificate Validation Disabled"),
            ("SCS0005", "Weak Random Number Generator"),
            ("SCS0006", "Weak hashing function"),
            ("SCS0007", "XML eXternal Entity Injection (XXE)"),
            ("SCS0008", "Cookie Without SSL Flag"),
            ("SCS0009", "Cookie Without HttpOnly Flag"),
            ("SCS0010", "Weak cipher algorithm"),
            ("SCS0011", "Unsafe XSLT setting used"),
            ("SCS0012", "Controller method is potentially vulnerable to authorization bypass"),
            ("SCS0013", "Potential usage of weak CipherMode mode"),
            ("SCS0015", "Hardcoded Password"),
            ("SCS0016", "Cross-Site Request Forgery (CSRF)"),
            ("SCS0017", "Request Validation Disabled (Attribute)"),
            ("SCS0018", "Path Traversal"),
            ("SCS0019", "OutputCache Conflict"),
            ("SCS0021", "Request Validation Disabled (Configuration File)"),
            ("SCS0022", "Event Validation Disabled"),
            ("SCS0023", "View State Not Encrypted"),
            ("SCS0024", "View State MAC Disabled"),
            ("SCS0026", "LDAP Distinguished Name Injection"),
            ("SCS0027", "Open Redirect"),
            ("SCS0028", "Insecure Deserialization"),
            ("SCS0029", "Cross-Site Scripting (XSS)"),
            ("SCS0030", "Request validation is enabled only for pages (Configuration File)"),
            ("SCS0031", "LDAP Filter Injection"),
            ("SCS0032", "Password RequiredLength Too Small"),
            ("SCS0033", "Password Complexity"),
            ("SCS0034", "Password RequiredLength Not Set"),
        }.ToLookup(v => v.Item1, v => v.Item2);

        public static void Main(params string[] args)
        {
            var report = JsonConvert.DeserializeObject<SecurityCodeScanOutput>(File.ReadAllText(args[0]));

            var run = report?.Runs?.FirstOrDefault();

            var output = new GitLabSastResultOutput {
                Version = "2.0",
                Vulnerabilities = run?.Results?.ToList().Select(r => {
                    var location = r?.Locations?.FirstOrDefault()?.PhysicalLocation;
                    var rule = run?.Tool?.Driver?.Rules?.FirstOrDefault(v => v.Id == r?.RuleId);
                    return new GitLabSastResultOutput.Vulnerability {
                        Id = r?.RuleId,
                        Name = RuleReference[r?.RuleId ?? ""]?.FirstOrDefault() ?? "Unknown",
                        Category = rule?.Properties?.Category,
                        Message = rule?.ShortDescription?.Text,
                        Description = r?.Message?.Text,
                        Severity = "Unknown",
                        Scanner = new GitLabSastResultOutput.Vulnerability.ScannerData {
                            Id = "security-code-scanner",
                            Name = "Security Code Scanner"
                        },
                        Location = new GitLabSastResultOutput.Vulnerability.LocationData {
                            File = $"./{location?.ArtifactLocation?.Uri}",
                            StartLine = location?.Region?.StartLine,
                            EndLine = location?.Region?.EndLine
                        },
                        Identifiers = new GitLabSastResultOutput.Vulnerability.Identifier[] {
                            new GitLabSastResultOutput.Vulnerability.Identifier {
                                Type = "security-code-scanner",
                                Name = $"Security Code Scanner - {r?.RuleId}",
                                Value = r?.RuleId?.Substring(3),
                                Url = $"https://security-code-scan.github.io/#{r?.RuleId}"
                            }
                        }
                    };
                }).ToArray()
            };

            File.WriteAllText("./gl-sast-report.json", JsonConvert.SerializeObject(output, Formatting.Indented));
        }

        public class GitLabSastResultOutput
        {
            public string? Version { get; set; }
            public Vulnerability[]? Vulnerabilities { get; set; }

            public class Vulnerability
            {
                public string? Id { get; set; }
                public string? Category { get; set; }
                public string? Name { get; set; }
                public string? Message { get; set; }
                public string? Description { get; set; }
                public string? Severity { get; set; }
                public string? Confidence { get; set; }
                public ScannerData? Scanner { get; set; }
                public LocationData? Location { get; set; }
                public Identifier[]? Identifiers { get; set; }

                public class ScannerData
                {
                    public string? Id { get; set; }
                    public string? Name { get; set; }
                }

                public class LocationData
                {
                    public string? File { get; set; }
                    public int? StartLine { get; set; }
                    public int? EndLine { get; set; }
                }

                public class Identifier
                {
                    public string? Type { get; set; }
                    public string? Name { get; set; }
                    public string? Value { get; set; }
                    public string? Url { get; set; }
                }
            }
        }
    
        public class SecurityCodeScanOutput
        {
            public Run[]? Runs { get; set; }

            public class Run
            {
                public Result[]? Results { get; set; }
                public ToolData? Tool { get; set; }

                public class Result
                {
                    public string? RuleId { get; set; }
                    public int? RuleIndex { get; set; }
                    public string? Level { get; set; }
                    public PropertyData? Properties { get; set; }
                    public MessageData? Message { get; set; }
                    public Location[]? Locations { get; set; }

                    public class PropertyData
                    {
                        public int? WarningLevel { get; set; }
                    }

                    public class MessageData
                    {
                        public string? Text { get; set; }
                    }

                    public class Location
                    {
                        public PhysicalLocationData? PhysicalLocation { get; set; }

                        public class PhysicalLocationData
                        {
                            public ArtifactLocationData? ArtifactLocation { get; set; }
                            public RegionData? Region { get; set; }

                            public class ArtifactLocationData
                            {
                                public string? Uri { get; set; }
                            }

                            public class RegionData
                            {
                                public int? StartLine { get; set; }
                                public int? StartColumn { get; set; }
                                public int? EndLine { get; set; }
                                public int? EndColumn { get; set; }
                            }
                        }
                    }
                }

                public class ToolData
                {
                    public DriverData? Driver { get; set; }

                    public class DriverData
                    {
                        public string? Name { get; set; }
                        public Rule[]? Rules { get; set; }
                        
                        public class Rule
                        {
                            public string? Id { get; set; }
                            public ShortDescriptionData? ShortDescription { get; set; }
                            public PropertyData? Properties { get; set; }

                            public class ShortDescriptionData
                            {
                                public string? Text { get; set; }
                            }

                            public class PropertyData
                            {
                                public string? Category { get; set; }
                            }
                        }
                    }
                }
            }
        }
    }
}